#!/usr/bin/env python3 

from flask import Flask, request
from flask_restful import Resource, Api
import numpy as np
import os
#from NumberPlateRecognition import plate_recognition as pr
app = Flask(__name__)
api = Api(app)
import logging
log = logging.getLogger('werkzeug')
log.setLevel(logging.ERROR)
class Server(Resource):
    def post(self):
        req = request.data.decode('utf-8')
        print( req )
        return 'Done'

api.add_resource(Server, '/')

if __name__ == '__main__':
    app.run(host = '0.0.0.0', port = 3398, debug = True)
